/* Copyright (C) 2013-2014, Hsiang Kao (e0e1e) <esxgx@163.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
#ifndef ___PERROR_H
#define ___PERROR_H

#include <stdio.h>

#define _perror(...)	do{\
	char buf[256];			\
	sprintf(buf, __VA_ARGS__);	\
	perror(buf);			\
}while(0)

#endif